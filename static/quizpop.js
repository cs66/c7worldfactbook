fetch('static/worldl.json')
  .then(r=>r.json())
  .then(r=>{
      let q = document.createElement('div');
      q.innerText = 'Which country has the biggest population?';
      document.getElementById('question').append(q);
      let alist = r.filter(c=>c.population >= 100000000);
      for(let c of alist){
        c.rand = Math.random();
      }
      alist.sort((a,b)=>a.rand-b.rand);
      let distractors = [];
      for(let i=0;i<4;i++){
          distractors.push(alist[i]);
      }
      //Find biggest population
      let biggest = 0;
      for (let c of distractors){
          if (c.population>biggest){
              biggest = c.population;
          }
      }
      console.log(biggest);
      for(let c of distractors){
          let d = document.createElement('div');
          d.innerText = c.name;
          d.onclick = ()=>{
              if (c.population === biggest){
                  alert("You are right!");
              }else{
                  alert("That is not right");
              }
          };
          document.getElementById('question').append(d);
      }
  })
